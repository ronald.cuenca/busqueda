/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import javax.swing.table.AbstractTableModel;
import modelo.persona;

/**
 *
 * @author Ronald Cuenca, Andres Guachon
 */
public class tablaPersona extends AbstractTableModel{
    ListaEnlazada<persona> lista;

    public ListaEnlazada<persona> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<persona> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }
    
    @Override
    public String getColumnName(int column){
        switch (column) {
            case 0: return "Id";
            case 1: return "Nombre";
            case 2: return "Edad";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        persona autor = lista.obtenerDato(rowIndex);
        switch (columnIndex) {
            case 0: return autor.getIDPersona();
            case 1: return autor.getNombre();
            case 2: return autor.getEdad();
            default: return null;
        }
    }
}
